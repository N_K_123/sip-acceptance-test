package uk.co.rantandrave;

import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

import net.thucydides.jbehave.ThucydidesJUnitStory;
import uk.co.rantandrave.steps.SipTrunkSteps;

public class SipTrunk extends ThucydidesJUnitStory {
 
    @Override
    public InjectableStepsFactory stepsFactory() {
    	final SipTrunkSteps steps = new SipTrunkSteps();
    	return new InstanceStepsFactory(new MostUsefulConfiguration(), steps);
    }
}
